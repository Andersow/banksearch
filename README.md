# Bank Search - teste para análise de competências


## Features
* É possível listar a partir do webservice.
* Filtrar as instituições.
* Ao filtrar por uma instituição que não existe na lista, é dado um tratamento especial.
* É possível adicionar aos favoritos e remover.


```
Nykel Andersow Costa Lima
nykelandersow@gmail.com
11 958341340

Analista Desenvolvedor Java e Android Sr
```