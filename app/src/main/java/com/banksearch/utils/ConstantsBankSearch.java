package com.banksearch.utils;

public final class ConstantsBankSearch {

    public static final String TAG = "LISTBANK";
    public static final String MSG_SUCCESS = "Listagem de bancos realizada com sucesso!";
    public static final String MSG_FAILURE = "Falha ao tentar listar os bancos";


    public static final String  BASE_URL_BANKS = "https://private-anon-3c9bb39de5-tqiandroid.apiary-mock.com/";

    private ConstantsBankSearch(){}
}
