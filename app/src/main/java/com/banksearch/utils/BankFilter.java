package com.banksearch.utils;

import com.banksearch.rest.BankRest;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class BankFilter {

    public List<BankRest> filter(String str, List<BankRest> banks) {

        List<BankRest> filtered = new ArrayList<BankRest>(new ArrayList<BankRest>(banks));

        ListIterator<BankRest> li = filtered.listIterator();
        while (li.hasNext()) {
            BankRest bank = li.next();

            if (!bank.getName().trim().toLowerCase().startsWith(str.trim().toLowerCase()))
                li.remove();
        }
        return filtered;
    }
}
