package com.banksearch.callbacks.exceptions;

public class ListBankFailureException extends Exception {
    public ListBankFailureException(String message)
    {
        super(message);
    }
}
