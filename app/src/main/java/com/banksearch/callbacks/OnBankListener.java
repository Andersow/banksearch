package com.banksearch.callbacks;

import com.banksearch.rest.BankRest;
import java.util.List;

public interface OnBankListener {
    void successEvent(List<BankRest> banks);
    void failureEvent(String msg);
}
