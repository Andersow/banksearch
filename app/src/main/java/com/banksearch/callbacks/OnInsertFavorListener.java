package com.banksearch.callbacks;

import com.banksearch.rest.BankRest;

import java.util.List;

public interface OnInsertFavorListener {
    void update();
}
