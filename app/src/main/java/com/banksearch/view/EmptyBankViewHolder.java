package com.banksearch.view;

import android.view.View;
import android.widget.TextView;

import com.banksearch.R;
import com.banksearch.rest.BankRest;

import java.util.List;

public class EmptyBankViewHolder extends BaseBankViewHolder {

    private TextView emtyMessage;

    public EmptyBankViewHolder(View itemView) {
        super(itemView);
        emtyMessage = itemView.findViewById(R.id.emptymessage);
    }

    @Override
    public void onBind(int position) {
        super.onBind(position);
        emtyMessage.setText("Sem instituição para listar.");
    }

    @Override
    protected void clear() {
        emtyMessage.setText("");
    }
}
