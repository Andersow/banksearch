package com.banksearch.view;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.banksearch.R;
import com.banksearch.rest.BankRest;
import java.util.List;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class BankAdapter extends RecyclerView.Adapter<BaseBankViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private BankAdapterCallback bankAdapterCallback;

    private List<BankRest> banks;

    public BankAdapter(List<BankRest> banks) {
        this.banks = banks;
    }

    @NonNull
    @Override
    public BaseBankViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new BankViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false),banks);
            case VIEW_TYPE_EMPTY:
            default:
                return new EmptyBankViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_empty, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (banks != null &&  banks.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull BaseBankViewHolder holder, int position) {
        holder.onBind(position);
    }

    public void addItems(List<BankRest> banks) {
        this.banks.addAll(banks);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if( banks != null && banks.size() > 0) {
            return banks.size();
        } else {
            return 1;
        }
    }

    public interface BankAdapterCallback {
        void onEmptyViewRetryClick();
    }

    public void setBankAdapterCallback(BankAdapterCallback bankAdapterCallback) {
        this.bankAdapterCallback = bankAdapterCallback;
    }
}
