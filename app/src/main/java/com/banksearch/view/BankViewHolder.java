package com.banksearch.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.banksearch.MainActivity;
import com.banksearch.R;
import com.banksearch.rest.BankRest;
import com.banksearch.rest.model.Bank;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class BankViewHolder extends BaseBankViewHolder {

    private ImageView imageBank;
    private ImageView imageStar;
    private TextView nameBank;
    private TextView codeBank;

    private List<BankRest> banks;

    public BankViewHolder(View itemView, List<BankRest> banks) {
        super(itemView);
        this.banks = banks;
        imageBank = itemView.findViewById(R.id.image);
        imageStar = itemView.findViewById(R.id.star);
        nameBank = itemView.findViewById(R.id.name);
        codeBank = itemView.findViewById(R.id.code);
    }

    @Override
    protected void clear() {
        imageBank.setImageDrawable(null);
        imageStar.setImageDrawable(null);
        nameBank.setText("");
        codeBank.setText("");
    }

    @Override
    public void onBind(int position) {
        super.onBind(position);

        final BankRest bank = banks.get(position);
        itemView.setTag(bank);

        if (bank.getName() != null) {
            nameBank.setText(bank.getName());
        }

        if (bank.getCode() != null) {
            codeBank.setText(bank.getCode());
        }

        Glide.with(itemView.getContext())
                .load(bank.getImageName())
                .placeholder(R.drawable.no_image)
                .into(imageBank);

        Glide.with(itemView.getContext())
                .load((bank.getFavorite() ? R.drawable.star : R.drawable.star_disable))
                .into(imageStar);

        imageStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imageStar = itemView.findViewById(R.id.star);
                BankRest bank = (BankRest) itemView.getTag();

                boolean add = (bank.getFavorite() == false);

                bank.setFavorite(add);

                Glide.with(view.getContext())
                        .load((add ? R.drawable.star : R.drawable.star_disable))
                        .into(imageStar);

                Snackbar snackbar = Snackbar.make(view, bank.getName() + String.format(" %s favoritos.", (add ? "adicionado como " : "removido dos")), Snackbar.LENGTH_LONG);
                View snackBarView = snackbar.getView();
                snackBarView.setBackgroundColor(view.getResources().getColor(add ? R.color.snackbarFavoriteBackgroundColor : R.color.snackbarNoFavoriteBackgroundColor));

                snackbar.show();

                MainActivity mainActivity = (MainActivity) view.getContext();

                mainActivity.loadDataBanks(mainActivity.getBanks());
            }
        });

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent transferir = new Intent("TRANSFERIR");

                BankRest bankRest = (BankRest) itemView.getTag();

                Bank bank = new Bank(bankRest.getName(), bankRest.getCode(), bankRest.getFavorite(), bankRest.getImageName());

                Bundle parametrosEnviar = new Bundle();
                parametrosEnviar.putParcelable(Bank.class.getName(), bank);
                transferir.putExtras(parametrosEnviar);

                view.getContext().startActivity(transferir);
            }
        });
    }
}
