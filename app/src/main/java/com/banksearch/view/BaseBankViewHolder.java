package com.banksearch.view;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseBankViewHolder extends RecyclerView.ViewHolder {
    private int currentPosition;

    public BaseBankViewHolder(View itemView) {
        super(itemView);
    }

    protected abstract void clear();

    public void onBind(int currentPosition) {
       this.currentPosition = currentPosition;
       clear();
    }

    public int getCurrentPosition() {
        return currentPosition;
    }
}
