package com.banksearch;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.banksearch.callbacks.OnBankListener;
import com.banksearch.callbacks.OnInsertFavorListener;
import com.banksearch.rest.BankRest;
import com.banksearch.services.impl.BankLister;
import com.banksearch.utils.BankFilter;
import com.banksearch.utils.ConstantsBankSearch;
import com.banksearch.view.BankAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements OnBankListener, OnInsertFavorListener {

    private EditText editValue;
    private List<BankRest> banks;

    private RecyclerView banksFavRecyclerView;
    private RecyclerView banksAnoRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        editValue = findViewById(R.id.editValue);
        editValue.setVisibility(View.GONE);

        banksFavRecyclerView = findViewById(R.id.banksFavRecyclerView);
        banksAnoRecyclerView = findViewById(R.id.banksAnoRecyclerView);

       final FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editValue.getVisibility() == View.GONE) {
                    editValue.setVisibility(View.VISIBLE);
                    fab.setImageResource(R.drawable.baseline_close_white_18dp);
                    fab.setBackgroundColor(view.getResources().getColor(R.color.actionRemoveFilter));
                } else {
                    editValue.setVisibility(View.GONE);
                    fab.setImageResource(R.drawable.baseline_filter_list_white_18dp);
                    fab.setBackgroundColor(view.getResources().getColor(R.color.colorAccent));
                }

                editValue.setText("");
            }
        });

        OnBankListener onBankListener = ((OnBankListener) this);
        BankLister bankLister = new BankLister();
        bankLister.registerBankListSuccessListener(onBankListener);

        bankLister.listBanksOrdered();

        editValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (banks == null)
                    return;

                if ((s != null) && (!s.toString().isEmpty())) {

                    BankFilter filter = new BankFilter();
                    loadDataBanks(filter.filter(s.toString(), banks));
                } else {
                    loadDataBanks(banks);
                }
            }
        });
    }


    @Override
    public void successEvent(List<BankRest> banks) {
        Log.i(ConstantsBankSearch.TAG, ConstantsBankSearch.MSG_SUCCESS);

        View view = findViewById(R.id.content);
        Snackbar.make(view, ConstantsBankSearch.MSG_SUCCESS, Snackbar.LENGTH_LONG).show();

        this.banks = banks;
        loadDataBanks(banks);
    }

    public void loadDataBanks(List<BankRest> banks) {
        List<BankRest> banksFavorites = new ArrayList<BankRest>();
        List<BankRest> banksOthers = new ArrayList<BankRest>();

        ListIterator<BankRest> li = banks.listIterator();
        while (li.hasNext()) {
            BankRest bank = li.next();
            if (bank.getFavorite())
                banksFavorites.add(bank);
            else
                banksOthers.add(bank);
        }

        populateRecycleView(banksFavRecyclerView, banksFavorites);
        populateRecycleView(banksAnoRecyclerView, banksOthers);
    }

    private void populateRecycleView(RecyclerView recyclerView, List<BankRest> banks) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        BankAdapter bankAdapter = new BankAdapter(new ArrayList<BankRest>());
        bankAdapter.addItems(banks);

        recyclerView.setAdapter(bankAdapter);
    }

    @Override
    public void failureEvent(String msg) {
        Log.e(ConstantsBankSearch.TAG, msg);
        View view = findViewById(R.id.content);
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();

    }

    @Override
    public void update() {

    }

    public List<BankRest> getBanks() {
        return banks;
    }
}
