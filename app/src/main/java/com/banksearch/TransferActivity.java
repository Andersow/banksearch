package com.banksearch;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.banksearch.rest.BankRest;
import com.banksearch.rest.model.Bank;
import com.bumptech.glide.Glide;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class TransferActivity extends AppCompatActivity {
    private List<BankRest> banks;

    private Bank bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Activity activity = this;

        Intent intent = getIntent();
        Bundle parametrosRecebidos = intent.getExtras();

        if (parametrosRecebidos != null)
            bank = parametrosRecebidos.getParcelable(Bank.class.getName());

        if (bank != null) {
            ((TextView) findViewById(R.id.name)).setText(bank.getName());
            ((TextView) findViewById(R.id.code)).setText(bank.getCode());

            final TextView editValue = findViewById(R.id.editValue);

            Glide.with(this)
                    .load(bank.getImageName())
                    .placeholder(R.drawable.no_image)
                    .into((ImageView) findViewById(R.id.image));


            Glide.with(this)
                    .load((bank.getFavorite() ? R.drawable.star : R.drawable.star_disable))
                    .into(((ImageView) findViewById(R.id.star)));
            ((Button) findViewById(R.id.sendTransfer)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!editValue.getText().toString().isEmpty())
                        Toast.makeText(activity, editValue.getText().toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
