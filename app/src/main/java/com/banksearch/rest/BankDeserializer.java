package com.banksearch.rest;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;

public class BankDeserializer implements JsonDeserializer<BankRest> {
    @Override
    public BankRest deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        BankRest bank = new BankRest(
                json.getAsJsonObject().get("name").getAsString(),
                json.getAsJsonObject().get("code").getAsString(),
                json.getAsJsonObject().get("favorite").getAsBoolean());

        String image = "";

        if (json.getAsJsonObject().has("imageName"))
            image = json.getAsJsonObject().get("imageName").getAsString();
        else if (json.getAsJsonObject().has("image"))
            image = json.getAsJsonObject().get("image").getAsString();

        bank.setImageName(image);

        return bank;
    }
}
