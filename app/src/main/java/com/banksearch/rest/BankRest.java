package com.banksearch.rest;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Objects;

@JsonAdapter(BankDeserializer.class)
public class BankRest implements Serializable {
    @SerializedName("name")
    private String name;

    @SerializedName("code")
    private String code;

    @SerializedName("favorite")
    private Boolean favorite;

    @SerializedName("imageName")
    private String imageName;

    public BankRest() {
    }

    public BankRest(String name, String code, Boolean favorite) {
        this.name = name;
        this.code = code;
        this.favorite = favorite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankRest bank = (BankRest) o;
        return name.equals(bank.name) &&
                code.equals(bank.code) &&
                favorite.equals(bank.favorite) &&
                imageName.equals(bank.imageName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, code, favorite, imageName);
    }
}
