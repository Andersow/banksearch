package com.banksearch.rest.model;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class Bank implements Parcelable {
    private String name;
    private String code;
    private Boolean favorite;
    private String imageName;

    public Bank() {
    }

    public Bank(String name, String code, Boolean favorite, String imageName) {
        this.name = name;
        this.code = code;
        this.favorite = favorite;
        this.imageName = imageName;
    }

    public Bank(Parcel parcel) {
        this.name = parcel.readString();
        this.code = parcel.readString();
        this.favorite = parcel.readInt() == 1;
        this.imageName = parcel.readString();
    }


    public static final Creator<Bank> CREATOR = new Creator<Bank>() {
        @Override
        public Bank createFromParcel(Parcel in) {
            return new Bank(in);
        }

        @Override
        public Bank[] newArray(int size) {
            return new Bank[size];
        }
    };

    @Override
    public int hashCode() {
        return Objects.hash(name, code, favorite, imageName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.code);
        dest.writeInt(this.getFavorite().booleanValue() ? 1 : 0);
        dest.writeString(this.imageName);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return name.equals(bank.name) &&
                code.equals(bank.code) &&
                favorite.equals(bank.favorite) &&
                imageName.equals(bank.imageName);
    }
}
