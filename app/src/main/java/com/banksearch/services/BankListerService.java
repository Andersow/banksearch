package com.banksearch.services;

import com.banksearch.rest.BankRest;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface BankListerService {
        @GET("list")
        Call<List<BankRest>> listBanks();
}
