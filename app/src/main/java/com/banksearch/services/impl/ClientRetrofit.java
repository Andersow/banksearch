package com.banksearch.services.impl;

import com.banksearch.utils.ConstantsBankSearch;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ClientRetrofit {

    private static Retrofit instance;

    private ClientRetrofit() {
    }

    public synchronized static Retrofit getInstance() {
        if(instance == null){
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            instance = new Retrofit.Builder().baseUrl(ConstantsBankSearch.BASE_URL_BANKS)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return instance;
    }
}