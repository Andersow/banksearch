package com.banksearch.services.impl;

import com.banksearch.rest.BankRest;
import com.banksearch.callbacks.OnBankListener;
import com.banksearch.callbacks.exceptions.ListBankFailureException;
import com.banksearch.services.BankListerService;
import com.banksearch.utils.ConstantsBankSearch;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public final class BankLister {
    private OnBankListener onBankListener;

    public void  listBanksOrdered(){
        BankListerService service =  ClientRetrofit.getInstance().create(BankListerService.class);

        Call<List<BankRest>> callListBanks = service.listBanks();

        try {
            callListBanks.enqueue(new Callback<List<BankRest>>() {
                @Override
                public void onResponse(Call<List<BankRest>> call, Response<List<BankRest>> response) {
                    try{
                        int statusCode = response.code();
                        if(statusCode != 200)
                            throw new ListBankFailureException(ConstantsBankSearch.MSG_FAILURE);

                        if (response.body() != null && statusCode == 200) {
                            List<BankRest> listBanks = response.body();
                            onBankListener.successEvent(listBanks);
                        }
                    }catch (ListBankFailureException e){
                        onBankListener.failureEvent(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<List<BankRest>> call, Throwable t) {
                    onBankListener.failureEvent(ConstantsBankSearch.MSG_FAILURE);
                }
            });
        } catch (Exception ex) {
            onBankListener.failureEvent(ConstantsBankSearch.MSG_FAILURE);
        }
    }

    public void registerBankListSuccessListener(OnBankListener onBankListener)
    {
        this.onBankListener = onBankListener;
    }
}
