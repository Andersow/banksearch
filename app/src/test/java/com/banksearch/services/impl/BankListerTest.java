package com.banksearch.services.impl;

import com.banksearch.MainActivity;
import com.banksearch.callbacks.OnBankListener;

import org.junit.Test;

import static org.junit.Assert.*;

public class BankListerTest {

    @Test
    public void listBanksOrdered() {
        BankLister lister = new BankLister();

        OnBankListener onBankListener = (OnBankListener) new MainActivity();
        lister.registerBankListSuccessListener(onBankListener);
        lister.listBanksOrdered();
    }
}